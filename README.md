# LOWDB as a simple local database server

Based on the [lowdb](https://github.com/typicode/lowdb) package.

My remote origin is on [Gitlab](https://gitlab.com/madrus_utils/lowdb-server)
