import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import { join, dirname } from 'path'
import { Low, JSONFile } from 'lowdb'
import { fileURLToPath } from 'url'

type Notes = {
  notes: string[]
}

const __dirname = dirname(fileURLToPath(import.meta.url))

// Use JSON file for storage
const file = join(__dirname, 'db.json')
const adapter = new JSONFile<Notes>(file)
const db = new Low<Notes>(adapter)

// Read data from JSON file, this will set db.data content
await db.read()
// If `db.json` file doesn't exist, db.data will be null
// Set default data
db.data ||= { notes: [] }

const app = express()
app.use(cors())
app.use(bodyParser.json())
const port = 4000

app.get('/notes', async (req, res) => {
  await db.read()
  const { notes } = db.data
  console.log('---------', notes)
  return res.json(notes)
})

app.post('/notes', async (req, res) => {
  db.data.notes = req.body
  console.log('---------', db.data)
  await db.write()
  res.json({ success: true })
})

app.listen(port, () =>
  console.log(`Backend running on http://localhost:${port}!`)
)
